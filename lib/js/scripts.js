'use strict'

angular.module('sosb', [])
// Unlike BadController, GoodController1 and GoodController2 will not fail to be instantiated,
// due to using explicit annotations using the array style and $inject property, respectively.
.controller('SosbVideos', ['$scope', function($scope) {
	var videos = ['sea', 'river'];
	var index = 0;
  $scope.display = {video: 'lib/vid/' + videos[index] + '.mp4'};

  $scope.increment = function() {
    index += 1;
    if (index >= videos.length) {
    	index = 0;
    }
  	$scope.display.video = 'lib/vid/' + videos[index] + '.mp4';
  }

  $scope.decrement = function() {
    index -= 1;
    if (index < 0) {
    	index = videos.length - 1;
    }
  	$scope.display.video = 'lib/vid/' + videos[index] + '.mp4';
  }
}])

$(document).ready(function(){
	var movers = $('*').filter(function () {
	    return this.className.match(/js-/);
	});
	movers.each(function(){
		var mover = $(this);
		mover.addClass('js-in');
	});
})
